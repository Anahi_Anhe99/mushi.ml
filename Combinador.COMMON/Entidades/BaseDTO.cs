﻿using System;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Entidades
{
    public abstract class BaseDTO
    {
        public ObjectId Id { get; set; }
        public string Nombre { get; set; }
    }
}
