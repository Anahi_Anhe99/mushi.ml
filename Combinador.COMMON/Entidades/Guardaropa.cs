﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Entidades
{
    public class Guardaropa:BaseDTO
    {
        public List<Prenda> Prendas { get; set; }
        public Usuario Usuario { get; set; }

        public override string ToString()
        {
            return string.Format("Guardaropa de {0}", Usuario.Nombre);
        }
    }
}
