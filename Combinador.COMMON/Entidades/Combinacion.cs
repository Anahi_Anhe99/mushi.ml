﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Entidades
{
    public class Combinacion:BaseDTO
    {
        public List<Prenda> CombinacionRopa { get; set; }
        public DateTime Fecha { get; set; }
        public string Ocasion { get; set; }
        public string Clima { get; set; }
        public string Temporada { get; set; }
        public Usuario Usuario { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} ({2}) [{3}]", Ocasion, Clima, Fecha, Temporada);
        }
    }
}
