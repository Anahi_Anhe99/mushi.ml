﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Entidades
{
    public class Prenda:BaseDTO
    {
        public string Color { get; set; }
        public string Tipo { get; set; }
        public byte[] Imagen { get; set; }
        public string Categoria { get; set; }

        public override string ToString()
        {
            return string.Format("{0} [{1}, {2}] ({3})", Nombre, Color, Categoria, Tipo);
        }
    }
}
