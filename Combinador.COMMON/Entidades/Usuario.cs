﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Entidades
{
    public class Usuario : BaseDTO
    {
        public string Correo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public char Genero { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public Byte[] FotoPerfil { get; set; }

        public override string ToString()
        {
            return string.Format("{0} [{1}]", Nombre, Correo);
        }
    }
}
