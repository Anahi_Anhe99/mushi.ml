﻿using Combinador.COMMON.Entidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface IPrendaManager:IGenericManager<Prenda>
    {
        List<Prenda> ListarConExcepciones(List<Prenda> excepciones);
        IEnumerable BuscarPorTipo();
    }
}
