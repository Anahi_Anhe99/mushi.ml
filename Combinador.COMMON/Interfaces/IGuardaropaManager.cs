﻿using Combinador.COMMON.Entidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface IGuardaropaManager : IGenericManager<Guardaropa>
    {
        IEnumerable ListarPorUsuario(Usuario usuario);
        //List<Prenda> ListarConExcepciones(List<Prenda> prendasCombinacion);
    }
}
