﻿using Combinador.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface IUsuarioManager:IGenericManager<Usuario>
    {
    }
}
