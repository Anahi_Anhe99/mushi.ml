﻿using Combinador.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        string Error { get; set; }
        T Crear(T entidad);
        List<T> Listar { get; }
        T Modificar(T entidadModificada);
        bool Eliminar(ObjectId id);
        T BuscarPorId(ObjectId id);
    }
}
