﻿using Combinador.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        string Error { get; set; }
        bool Resultado { get; }
        T Create(T entidad);
        List<T> Read { get; }
        T Update(T entidadModificada);
        bool Delete(ObjectId id);
    }
}
