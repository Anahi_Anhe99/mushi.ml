﻿using Combinador.COMMON.Entidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Interfaces
{
    public interface ICombinacionManager:IGenericManager<Combinacion>
    {
        List<Combinacion> ListarPorUsuario(Usuario usuario);
        IEnumerable BuscarOcacion();
        IEnumerable BuscarClima();
    }
}
