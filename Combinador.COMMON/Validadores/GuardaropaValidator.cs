﻿using Combinador.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Validadores
{
    public class GuardaropaValidator:GenericValidator<Guardaropa>
    {
        public GuardaropaValidator()
        {
            RuleFor(p => p.Prendas).NotNull().NotEmpty();
            RuleFor(p => p.Usuario).NotNull().NotEmpty();
        }
    }
}
