﻿using Combinador.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Validadores
{
    public class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(p => p.Id).NotNull().NotEmpty();
            RuleFor(p => p.Nombre).NotNull().NotEmpty().Length(2, 50);
        }
    }
}
