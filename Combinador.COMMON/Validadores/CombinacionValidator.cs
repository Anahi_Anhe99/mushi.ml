﻿using Combinador.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Validadores
{
    public class CombinacionValidator:GenericValidator<Combinacion>
    {
        public CombinacionValidator()
        {
            RuleFor(p => p.Clima).NotNull().NotEmpty().Length(2, 20);
            RuleFor(p => p.CombinacionRopa).NotNull().NotEmpty();
            RuleFor(p => p.Fecha).NotNull().NotEmpty();
            RuleFor(p => p.Ocasion).NotNull().NotEmpty().Length(2, 30);
            RuleFor(p => p.Temporada).NotNull().NotEmpty().Length(2, 30);
            RuleFor(p => p.Usuario).NotNull().NotEmpty();
        }
    }
}
