﻿using Combinador.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Validadores
{
    public class UsuarioValidator:GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(p => p.Correo).NotNull().NotEmpty().Length(3, 90).EmailAddress();
            RuleFor(p => p.FechaNacimiento).NotNull().NotEmpty();
            RuleFor(p => p.Genero).NotNull().NotEmpty();
            RuleFor(p => p.Password).NotNull().NotEmpty().Length(5, 15);
            RuleFor(p => p.UserName).NotNull().NotEmpty().Length(5, 15);
        }
    }
}
