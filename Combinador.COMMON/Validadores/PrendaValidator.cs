﻿using Combinador.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.COMMON.Validadores
{
    public class PrendaValidator:GenericValidator<Prenda>
    {
        public PrendaValidator()
        {
            RuleFor(p => p.Categoria).NotNull().NotEmpty().Length(2, 20);
            RuleFor(p => p.Color).NotNull().NotEmpty().Length(2, 20);
            RuleFor(p => p.Imagen).NotNull().NotEmpty();
            RuleFor(p => p.Tipo).NotNull().NotEmpty().Length(2, 20);
        }
    }
}
