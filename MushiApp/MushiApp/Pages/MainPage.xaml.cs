﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MushiApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
            InitializeComponent();
            btnLogIn.Clicked += BtnLogIn_Clicked;
    }

        private async void BtnLogIn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PrincipalPage());
        }
    }
}