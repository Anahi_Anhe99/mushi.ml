﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public string Error { get; set; }
        public bool Resultado { get; private set; }
        AbstractValidator<T> validator;
        ValidationResult result;
        public GenericRepository(AbstractValidator<T> _validator)
        {
            client = new MongoClient(new MongoUrl("mongodb://mushidb:quesadillas99@ds046867.mlab.com:46867/mushidb"));
            db = client.GetDatabase("mushidb");
            validator = _validator;
        }

        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public List<T> Read => Collection().AsQueryable().ToList();

        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId();
            result = validator.Validate(entidad);
            if (result.IsValid)
            {
                try
                {
                    Collection().InsertOne(entidad);
                    Error = null;
                    Resultado = true;
                }
                catch (Exception)
                {
                    Error = "No se pudo crear " + typeof(T).Name;
                    Resultado = false;
                }
            }
            else
            {
                Resultado = false;
                Error = typeof(T).Name + " invalida:";
                foreach (var error in result.Errors)
                {
                    Error += "\n" + error.ErrorMessage;
                }
            }
            return Resultado ? entidad : null;
        }

        public bool Delete(ObjectId id)
        {
            try
            {
                Error = null;
                Resultado = Collection().DeleteOne(p => p.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {
                Error = "No se pudo eliminar " + typeof(T).Name;
                Resultado = false;
            }
            return Resultado;
        }

        public T Update(T entidadModificada)
        {
            result = validator.Validate(entidadModificada);
            if (result.IsValid)
            {
                try
                {
                    Error = null;
                    Resultado = Collection().ReplaceOne(p => p.Id == entidadModificada.Id, entidadModificada).ModifiedCount == 1;
                }
                catch (Exception)
                {
                    Error = "No se pudo modificar " + typeof(T).Name;
                    Resultado = false;
                }
            }
            else
            {
                Resultado = false;
                Error = typeof(T).Name + " invalida:";
                foreach (var error in result.Errors)
                {
                    Error += "\n" + error.ErrorMessage;
                }
            }
                return Resultado ? entidadModificada : null;
            }
        }
    }
