﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Combinador.BIZ
{
    public class CombinacionManager : GenericManager<Combinacion>, ICombinacionManager
    {
        public CombinacionManager(IGenericRepository<Combinacion> genericRepository) : base(genericRepository)
        {
        }

        public IEnumerable BuscarClima()
        {
            return Listar.Select(i => i.Clima).Distinct();
        }

        public IEnumerable BuscarOcacion()
        {
            return Listar.Select(i => i.Ocasion).Distinct();
        }

        public List<Combinacion> ListarPorUsuario(Usuario usuario)
        {
            return Listar.Where(p => p.Usuario == usuario).ToList();
        }
    }
}
