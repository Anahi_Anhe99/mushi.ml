﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Validadores;
using Combinador.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.BIZ
{
    public static class FabricManager
    {
        public static CombinacionManager Combinacion() => new CombinacionManager(new GenericRepository<Combinacion>(new CombinacionValidator()));

        public static GuardaropaManager Guardaropa() => new GuardaropaManager(new GenericRepository<Guardaropa>(new GuardaropaValidator()));

        public static PrendaManager Prenda() => new PrendaManager(new GenericRepository<Prenda>(new PrendaValidator()));

        public static UsuarioManager Usuario() => new UsuarioManager(new GenericRepository<Usuario>(new UsuarioValidator()));
    }
}
