﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Combinador.BIZ
{
    public class GuardaropaManager : GenericManager<Guardaropa>, IGuardaropaManager
    {
        public GuardaropaManager(IGenericRepository<Guardaropa> genericRepository) : base(genericRepository)
        {
        }
           

        public IEnumerable ListarPorUsuario(Usuario usuario)
        {
            return Listar.Where(p => p.Usuario == usuario).ToList();
        }
    }
}
