﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Combinador.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> genericRepository)
        {
            repository = genericRepository;
        }

        public string Error { get => repository.Error; set { } }

        public List<T> Listar => repository.Read;

        public T BuscarPorId(ObjectId id) => Listar.Where(p => p.Id == id).SingleOrDefault();

        public T Crear(T entidad) => repository.Create(entidad);

        public bool Eliminar(ObjectId id) => repository.Delete(id);

        public T Modificar(T entidadModificada) => repository.Update(entidadModificada);
    }
}
