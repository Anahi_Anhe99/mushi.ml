﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Combinador.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        public UsuarioManager(IGenericRepository<Usuario> genericRepository) : base(genericRepository)
        {
        }
    }
}
