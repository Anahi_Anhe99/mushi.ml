﻿using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Combinador.BIZ
{
    public class PrendaManager : GenericManager<Prenda>, IPrendaManager
    {
        public PrendaManager(IGenericRepository<Prenda> genericRepository) : base(genericRepository)
        {
        }

        public IEnumerable BuscarPorTipo()
        {
            return Listar.Select(i => i.Tipo).Distinct();
        }

        public List<Prenda> ListarConExcepciones(List<Prenda> excepciones)
        {
            List<Prenda> listado = new List<Prenda>();
            foreach (var prenda in Listar)
            {
                if (excepciones.Where(p => p.Id == prenda.Id).Count() == 0)
                {
                    listado.Add(prenda);
                }
            }
            return listado;
        }
    }
}
