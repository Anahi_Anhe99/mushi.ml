﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Combinador.GUI.Prueba.Interfaces.GUI
{
    public interface IRepositorioPanel
    {
        string Error { get; }
        string SegundoError { get; set; }
        string NombreEntidad { get; }
        void ModoEdicion();
        void Limpiar();
        bool Eliminar();
        bool GuardarNuevo();
        DataGrid AsingnarDataGrid { set; }
        void MostrarDatos();
        bool GuardarModificado();
    }
}
