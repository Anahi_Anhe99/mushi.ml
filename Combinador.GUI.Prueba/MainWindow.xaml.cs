﻿using Combinador.GUI.Prueba.Controles;
using Combinador.GUI.Prueba.Interfaces.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinador.GUI.Prueba
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Accion
        {
            Nuevo,
            Editar
        };
        IRepositorioPanel panel;
        Accion accion;
        public MainWindow()
        {
            InitializeComponent();
            txbEncabezadoEntidad.Text = "Selecciona el modulo que deseas ver";
            PanelEdicionVisible(false);
        }

        #region MetodosManejoGeneralVentana
        private string ObtenerError()
        {
            if (panel.SegundoError != "")
            {
                return panel.SegundoError;
            }
            else
            {
                return panel.Error;
            }
        }

        private void PanelEdicionVisible(bool v)
        {
            dtgDatos.Visibility = v ? Visibility.Visible : Visibility.Collapsed;
            wrpBotones.Visibility = v ? Visibility.Visible : Visibility.Collapsed;
            grdContenedor.Children.Clear();
        }

        private void CambiarCamposContenedor(UserControl control)
        {
            grdContenedor.Children.Add(control);
            panel = control as IRepositorioPanel;
            panel.AsingnarDataGrid = dtgDatos;
            panel.MostrarDatos();
            PanelEnEdicion(false);
        }

        private void PanelEnEdicion(bool v)
        {
            btnCancelar.IsEnabled = v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            grdContenedor.IsEnabled = v;
        } 
        #endregion

        #region ControladoresClikcParaBotonesPanel
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            accion = Accion.Nuevo;
            PanelEnEdicion(true);
            panel.Limpiar();
            panel.MostrarDatos();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (panel.NombreEntidad != "")
            {
                accion = Accion.Editar;
                PanelEnEdicion(true);
                panel.ModoEdicion();
            }
            else
            {
                MessageBox.Show("No has seleccionado elemento a editar", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (panel.NombreEntidad != "")
            {
                if (MessageBox.Show("¿Deseas eliminar " + panel.NombreEntidad + "?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (panel.Eliminar())
                    {
                        panel.MostrarDatos();
                        panel.Limpiar();
                        MessageBox.Show("Elemento eliminado", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show(ObtenerError(), this.Title, MessageBoxButton.YesNo, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("No has seleccionado elemento a eliminar", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accion == Accion.Nuevo)
            {
                if (panel.GuardarNuevo())
                {
                    panel.MostrarDatos();
                    panel.Limpiar();
                    PanelEnEdicion(false);
                    MessageBox.Show("Elemento guardado con exito", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show(ObtenerError(), this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                if (panel.GuardarModificado())
                {
                    panel.MostrarDatos();
                    panel.Limpiar();
                    PanelEnEdicion(false);
                    MessageBox.Show("Elemento modificado con exito", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show(ObtenerError(), this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            PanelEnEdicion(false);
            panel.MostrarDatos();
            panel.Limpiar();
        } 
        #endregion

        #region ControladoresClickMenuItems
        private void menuModulosOpcCombinaciones_Click(object sender, RoutedEventArgs e)
        {
            PanelEdicionVisible(true);
            txbEncabezadoEntidad.Text = "Combinaciones";
            CambiarCamposContenedor(new CombinacionControl());
        }

        private void menuModulosOpcGuardaropas_Click(object sender, RoutedEventArgs e)
        {
            PanelEdicionVisible(true);
            txbEncabezadoEntidad.Text = "Guardaropas";
            CambiarCamposContenedor(new GuardaropaControl());
        }

        private void menuModulosOpcPrendas_Click(object sender, RoutedEventArgs e)
        {
            PanelEdicionVisible(true);
            txbEncabezadoEntidad.Text = "Prendas";
            CambiarCamposContenedor(new PrendaControl());
        }

        private void menuModulosOpcUsuarios_Click(object sender, RoutedEventArgs e)
        {
            PanelEdicionVisible(true);
            txbEncabezadoEntidad.Text = "Usuarios";
            CambiarCamposContenedor(new UsuarioControl());
        }

        private void menuArchivoOpcSalir_Click(object sender, RoutedEventArgs e)
        {
            PanelEdicionVisible(false);
            if (MessageBox.Show("¿Realmente deseas salir?", this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
            else
            {
                txbEncabezadoEntidad.Text = "Selecciona el modulo que deseas ver";
            }
        }
        #endregion
    }
}
