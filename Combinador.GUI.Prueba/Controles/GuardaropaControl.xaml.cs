﻿using Combinador.BIZ;
using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using Combinador.GUI.Prueba.Interfaces.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinador.GUI.Prueba.Controles
{
    /// <summary>
    /// Lógica de interacción para GuardaropaControl.xaml
    /// </summary>
    public partial class GuardaropaControl : UserControl, IRepositorioPanel
    {
        DataGrid dtg;
        IGuardaropaManager guardaropas;
        IPrendaManager prendas;
        IUsuarioManager usuarios;
        List<Prenda> prendasGuardaropa;
        public GuardaropaControl()
        {
            InitializeComponent();
            guardaropas = FabricManager.Guardaropa();
            prendas = FabricManager.Prenda();
            usuarios = FabricManager.Usuario();
            prendasGuardaropa = new List<Prenda>();
            dtg = new DataGrid();
        }

        #region MetodosInterfaz
        public string Error => guardaropas.Error;

        public DataGrid AsingnarDataGrid { set => dtg = value; }

        public string SegundoError { get; set; }

        public string NombreEntidad => dtg.SelectedItem != null ? (dtg.SelectedItem as Guardaropa).Nombre : "";

        public bool Eliminar()
        {
            return guardaropas.Eliminar((dtg.SelectedItem as Guardaropa).Id);
        }

        public bool GuardarModificado()
        {
            Guardaropa guardaropa = dtg.SelectedItem as Guardaropa;
            guardaropa.Nombre = txbNombre.Text;
            guardaropa.Usuario = cmbUsuario.SelectedItem as Usuario;
            guardaropa.Prendas = prendasGuardaropa;
            SegundoError = "";
            return guardaropas.Modificar(guardaropa) != null;
        }

        public bool GuardarNuevo()
        {
            Guardaropa guardaropa = new Guardaropa()
            {
                Nombre = txbNombre.Text,
                Usuario = cmbUsuario.SelectedItem as Usuario,
                Prendas = prendasGuardaropa
            };
            SegundoError = "";
            return guardaropas.Crear(guardaropa) != null;
        }

        public void Limpiar()
        {
            btnQuitarPrenda.IsEnabled = false;
            dtgPrendasAgregadas.ItemsSource = null;
            cmbPrenda.Text = "";
            cmbUsuario.Text = "";
            txbId.Text = "";
            txbNombre.Clear();
            CargarPrendasExcepto();
            prendasGuardaropa = new List<Prenda>();
            SegundoError = "";
        }

        public void ModoEdicion()
        {
            Guardaropa guardaropa = dtg.SelectedItem as Guardaropa;
            txbId.Text = guardaropa.Id.ToString();
            txbNombre.Text = guardaropa.Nombre;
            cmbUsuario.Text = guardaropa.Usuario.ToString();
            prendasGuardaropa = guardaropa.Prendas;
            CargarPrendasExcepto(prendasGuardaropa);
            ActualizarTablaPrendas();
            if (prendasGuardaropa.Count > 0)
                btnQuitarPrenda.IsEnabled = true;
        }

        public void MostrarDatos()
        {
            dtg.ItemsSource = null;
            dtg.ItemsSource = guardaropas.Listar;
            cmbUsuario.ItemsSource = null;
            cmbUsuario.ItemsSource = usuarios.Listar;
            CargarPrendasExcepto();
        } 
        #endregion

        #region MetodosManejoGeneralControl
        private void CargarPrendasExcepto(List<Prenda> excepciones = null)
        {
            cmbPrenda.ItemsSource = null;
            if (excepciones == null)
            {
                cmbPrenda.ItemsSource = prendas.Listar;
            }
            else
            {
                cmbPrenda.ItemsSource = prendas.ListarConExcepciones(prendasGuardaropa);
            }
        }

        private void btnAgregarPrenda_Click(object sender, RoutedEventArgs e)
        {
            Prenda seleccionada = cmbPrenda.SelectedItem as Prenda;
            if (seleccionada != null)
            {
                prendasGuardaropa.Add(seleccionada);
                CargarPrendasExcepto(prendasGuardaropa);
                btnQuitarPrenda.IsEnabled = true;
                ActualizarTablaPrendas();
            }
        }

        private void ActualizarTablaPrendas()
        {
            dtgPrendasAgregadas.ItemsSource = null;
            dtgPrendasAgregadas.ItemsSource = prendasGuardaropa;
        }

        private void btnQuitarPrenda_Click(object sender, RoutedEventArgs e)
        {
            Prenda seleccionada = dtgPrendasAgregadas.SelectedItem as Prenda;
            if (seleccionada != null)
            {
                prendasGuardaropa.Remove(seleccionada);
                CargarPrendasExcepto(prendasGuardaropa);
                ActualizarTablaPrendas();
                if (prendasGuardaropa.Count == 0)
                    btnQuitarPrenda.IsEnabled = false;
            }
        } 
        #endregion
    }
}