﻿using Combinador.BIZ;
using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using Combinador.GUI.Prueba.Interfaces.GUI;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinador.GUI.Prueba.Controles
{
    /// <summary>
    /// Lógica de interacción para PrendaControl.xaml
    /// </summary>
    public partial class PrendaControl : UserControl, IRepositorioPanel
    {
        DataGrid dtg;
        IPrendaManager prendas;
        public PrendaControl()
        {
            InitializeComponent();
            dtg = new DataGrid();
            prendas = FabricManager.Prenda();
        }

        #region MetodosInterfaz
        public string Error => prendas.Error;

        public DataGrid AsingnarDataGrid { set => dtg = value; }

        public string SegundoError { get; set; }

        public string NombreEntidad => dtg.SelectedItem != null ? (dtg.SelectedItem as Prenda).Nombre : "";

        public bool Eliminar()
        {
            return prendas.Eliminar((dtg.SelectedItem as Prenda).Id);
        }

        public bool GuardarModificado()
        {
            Prenda prenda = dtg.SelectedItem as Prenda;
            prenda.Categoria = cmbCategoria.Text;
            prenda.Color = txbColor.Text;
            prenda.Nombre = txbNombre.Text;
            prenda.Tipo = cmbTipo.Text;
            prenda.Imagen = ImageToByte(imgImagen.Source);
            SegundoError = "";  
            return prendas.Modificar(prenda) != null;
        }

        public bool GuardarNuevo()
        {
            Prenda prenda = new Prenda()
            {
                Categoria = cmbCategoria.Text,
                Color = txbColor.Text,
                Nombre = txbNombre.Text,
                Tipo = cmbTipo.Text,
                Imagen = ImageToByte(imgImagen.Source)
            };
            SegundoError = ""; 
            return prendas.Crear(prenda) != null;
        }

        public void Limpiar()
        {
            cmbCategoria.SelectedItem = null;
            txbColor.Clear();
            txbId.Text = "";
            txbNombre.Clear();
            cmbTipo.SelectedItem=null;
            imgImagen.Source = null;
            SegundoError = "";
        }

        public void ModoEdicion()
        {
            Prenda prenda = dtg.SelectedItem as Prenda;
            cmbCategoria.Text = prenda.Categoria;
            txbColor.Text = prenda.Color;
            txbId.Text = prenda.Id.ToString();
            txbNombre.Text = prenda.Nombre;
            cmbTipo.Text = prenda.Tipo;
            imgImagen.Source = ByteToImage(prenda.Imagen);
        }

        public void MostrarDatos()
        {
            dtg.ItemsSource = null;
            dtg.ItemsSource = prendas.Listar;
            cmbTipo.ItemsSource = null;
            cmbTipo.ItemsSource = prendas.BuscarPorTipo();
            cmbCategoria.ItemsSource=null;
            cmbCategoria.Items.Add("Casual");
            cmbCategoria.Items.Add("Informal");
            cmbCategoria.Items.Add("Deportivo");
            cmbCategoria.Items.Add("Formal");
            cmbCategoria.Items.Add("Etiqueta");
        } 
        #endregion

        #region MetodosManejoGeneralControl
        private void btnElegirImagen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialogo = new OpenFileDialog();
            dialogo.Title = "Selecciona la imagen de la prenda";
            dialogo.Filter = "Archivos de imagen|*.jpg; *.png";
            if (dialogo.ShowDialog().Value)
            {
                imgImagen.Source = new BitmapImage(new Uri(dialogo.FileName));
            }
        }

        private byte[] ImageToByte(ImageSource image)
        {
            if (image != null)
            {
                MemoryStream memStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                encoder.Save(memStream);
                return memStream.ToArray();
            }
            else
            {
                return null;
            }
        }

        private ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData == null)
            {
                return null;
            }
            else
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();
                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
        }
        #endregion

        
    }
}
