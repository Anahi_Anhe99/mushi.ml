﻿using Combinador.BIZ;
using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using Combinador.GUI.Prueba.Interfaces.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinador.GUI.Prueba.Controles
{
    /// <summary>
    /// Lógica de interacción para UsuarioControl.xaml
    /// </summary>
    public partial class UsuarioControl : UserControl, IRepositorioPanel
    {
        DataGrid dtg;
        IUsuarioManager usuarios;
        public UsuarioControl()
        {
            InitializeComponent();
            usuarios = FabricManager.Usuario();
            dtg = new DataGrid();
        }

        #region MetodosInterfaz
        public string Error => usuarios.Error;

        public DataGrid AsingnarDataGrid { set => dtg = value; }

        public string SegundoError { get; set; }

        public string NombreEntidad => dtg.SelectedItem != null ? (dtg.SelectedItem as Usuario).Nombre : "";

        public bool Eliminar()
        {
            return usuarios.Eliminar((dtg.SelectedItem as Usuario).Id);
        }

        public bool GuardarModificado()
        {
            Usuario usuario = dtg.SelectedItem as Usuario;
            try
            {
                usuario.Correo = txbCorreo.Text;
                usuario.FechaNacimiento = pkrFechaNacimiento.SelectedDate.Value;
                usuario.Genero = char.Parse(cmbGenero.Text);
                usuario.Nombre = txbNombre.Text;
                usuario.Password = txbPassword.Text;
                usuario.UserName = txbUserName.Text;
                SegundoError = "";
                return usuarios.Modificar(usuario) != null;
            }
            catch (Exception)
            {
                SegundoError = "Selecciona fecha de nacimiento";
                return false;
            }
        }

        public bool GuardarNuevo()
        {
            try
            {
                Usuario usuario = new Usuario()
                {
                    Correo = txbCorreo.Text,
                    FechaNacimiento = pkrFechaNacimiento.SelectedDate.Value,
                    Genero = char.Parse(cmbGenero.Text),
                    Nombre = txbNombre.Text,
                    Password = txbPassword.Text,
                    UserName = txbUserName.Text
                };
                SegundoError = "";
                return usuarios.Crear(usuario) != null;
            }
            catch (Exception)
            {
                SegundoError = "Selecciona fecha de nacimiento";
                return false;
            }
        }

        public void Limpiar()
        {
            txbCorreo.Clear();
            txbId.Text = "";
            txbNombre.Clear();
            txbPassword.Clear();
            txbUserName.Clear();
            cmbGenero.Text = "";
            pkrFechaNacimiento.SelectedDate = null;
            dtg.SelectedItem = null;
            SegundoError = "";
        }

        public void ModoEdicion()
        {
            Usuario usuario = dtg.SelectedItem as Usuario;
            txbCorreo.Text = usuario.Correo;
            txbId.Text = usuario.Id.ToString();
            txbNombre.Text = usuario.Nombre;
            txbPassword.Text = usuario.Password;
            txbUserName.Text = usuario.UserName;
            cmbGenero.Text = usuario.Genero.ToString();
            pkrFechaNacimiento.SelectedDate = usuario.FechaNacimiento.Date;
        }

        public void MostrarDatos()
        {
            dtg.ItemsSource = null;
            dtg.ItemsSource = usuarios.Listar;
        } 
        #endregion
    }
}
