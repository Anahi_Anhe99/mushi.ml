﻿using Combinador.BIZ;
using Combinador.COMMON.Entidades;
using Combinador.COMMON.Interfaces;
using Combinador.GUI.Prueba.Interfaces.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinador.GUI.Prueba.Controles
{
    /// <summary>
    /// Lógica de interacción para CombinacionControl.xaml
    /// </summary>
    public partial class CombinacionControl : UserControl, IRepositorioPanel
    {
        DataGrid dtg;
        ICombinacionManager combinaciones;
        IGuardaropaManager guardaropa;
        IPrendaManager prendas;
        IUsuarioManager usuarios;
        List<Prenda> prendasCombinacion;
        public CombinacionControl()
        {
            InitializeComponent();
            combinaciones = FabricManager.Combinacion();
            guardaropa = FabricManager.Guardaropa();
            usuarios = FabricManager.Usuario();
            prendasCombinacion = new List<Prenda>();
            dtg = new DataGrid();
            cmbUsuario.ItemsSource = "";
            cmbUsuario.ItemsSource = usuarios.Listar;
        }

        #region MetodosInterfaz
        public string Error => combinaciones.Error;

        public DataGrid AsingnarDataGrid { set => dtg = value; }

        public string SegundoError { get; set; }

        public string NombreEntidad => dtg.SelectedItem != null ? (dtg.SelectedItem as Combinacion).Nombre : "";

        public bool Eliminar()
        {
            return combinaciones.Eliminar((dtg.SelectedItem as Combinacion).Id);
        }

        public bool GuardarModificado()
        {
            Combinacion combinacion = dtg.SelectedItem as Combinacion;
            try
            {
                combinacion.Clima = cmbClima.Text;
                combinacion.CombinacionRopa = prendasCombinacion;
                combinacion.Fecha = DateTime.Now;
                combinacion.Nombre = txbNombre.Text;
                combinacion.Ocasion = cmbOcasion.Text;
                combinacion.Temporada = cmbTemporada.Text;
                SegundoError = "";
                combinacion.Usuario = cmbUsuario.SelectedItem as Usuario;
                ActualizarTablaCombinaciones();
                return combinaciones.Modificar(combinacion) != null;

            }
            catch (Exception)
            {
                SegundoError = "Selecciona fecha";
                return false;
            }
        }

        public bool GuardarNuevo()
        {
            try
            {
                Combinacion combinacion = new Combinacion()
                {
                    Clima = cmbClima.Text,
                    CombinacionRopa = prendasCombinacion,
                    Fecha = DateTime.Now,
                    Nombre = txbNombre.Text,
                    Ocasion = cmbOcasion.Text,
                    Temporada = cmbTemporada.Text,
                    Usuario = cmbUsuario.SelectedItem as Usuario
                };
                ActualizarTablaCombinaciones();
                SegundoError = "";
                return combinaciones.Crear(combinacion) != null;
            }
            catch (Exception)
            {
                SegundoError = "Selecciona fecha";
                return false;
            }
        }

        public void Limpiar()
        {
            btnQuitarPrenda.IsEnabled = false;
            cmbClima.SelectedItem=null;
            txbId.Text = "";
            txbNombre.Clear();
            cmbOcasion.SelectedItem = null; ;
            CargarPrendasExcepto();
            prendasCombinacion = new List<Prenda>();
            dtgPrendasAgregadas.ItemsSource = null;
            SegundoError = "";
            cmbUsuario.SelectedItem = "";
            
        }

        public void ModoEdicion()
        {
            Combinacion combinacion = dtg.SelectedItem as Combinacion;
            cmbClima.Text = combinacion.Clima;
            txbId.Text = combinacion.Id.ToString();
            txbNombre.Text = combinacion.Nombre;
            cmbOcasion.Text = combinacion.Ocasion;
            cmbTemporada.Text = combinacion.Temporada;
            prendasCombinacion = combinacion.CombinacionRopa;
            cmbUsuario.SelectedItem = combinacion.Usuario;
            CargarPrendasExcepto(prendasCombinacion);
            ActualizarTablaPrendas();
            if (prendasCombinacion.Count > 0)
                btnQuitarPrenda.IsEnabled = true;
        }

        public void MostrarDatos()
        {
            dtg.ItemsSource = null;
            dtg.ItemsSource = combinaciones.Listar;
            cmbPrenda.SelectedItem = null;

            cmbTemporada.SelectedItem = null;
            cmbTemporada.Items.Add("Otoño - Invierno");
            cmbTemporada.Items.Add("primavera-verano");
            cmbClima.ItemsSource = combinaciones.BuscarClima();
            cmbOcasion.ItemsSource = combinaciones.BuscarOcacion();
        }
        #endregion

        #region MetodosManejoGeneralControl
        private void btnAgregarPrenda_Click(object sender, RoutedEventArgs e)
        {
            Prenda seleccionada = cmbPrenda.SelectedItem as Prenda;
            if (seleccionada != null)
            {
                prendasCombinacion.Add(seleccionada);
                CargarPrendasExcepto(prendasCombinacion);
                btnQuitarPrenda.IsEnabled = true;
                ActualizarTablaPrendas();
            }
        }

        private void btnQuitarPrenda_Click(object sender, RoutedEventArgs e)
        {
            Prenda seleccionada = dtgPrendasAgregadas.SelectedItem as Prenda;
            if (seleccionada != null)
            {
                prendasCombinacion.Remove(seleccionada);
                CargarPrendasExcepto(prendasCombinacion);
                ActualizarTablaPrendas();
                if (prendasCombinacion.Count == 0)
                    btnQuitarPrenda.IsEnabled = false;
            }
        }

        private void ActualizarTablaPrendas()
        {
            dtgPrendasAgregadas.ItemsSource = null;
            dtgPrendasAgregadas.ItemsSource = prendasCombinacion;
        }

        private void CargarPrendasExcepto(List<Prenda> excepciones = null)
        {
            //cmbPrenda.ItemsSource = null;
            //if (excepciones == null) 
            //{
            //    cmbPrenda.ItemsSource = guardaropa.ListarPorUsuario(cmbUsuario.SelectedItem as Usuario);
            //}
            //else
            //{
            //    cmbPrenda.ItemsSource = guardaropa.ListarPorUsuario(cmbUsuario.SelectedItem as Usuario);

            //}
        }
        #endregion 
        private void cmbUsuario_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ActualizarTablaCombinaciones();
        }

        private void ActualizarTablaCombinaciones()
        {
            dtg.ItemsSource = null;
            dtg.ItemsSource = combinaciones.ListarPorUsuario(cmbUsuario.SelectedItem as Usuario);
        }
    }
}
