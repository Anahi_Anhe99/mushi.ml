﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registros.Models
{
    public class LoginModel
    {
        public string Password { get; set; }
        public string Usuario { get; set; }
    }
}
