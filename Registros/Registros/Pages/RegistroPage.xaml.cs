﻿using Registros.Entidades;
using Registros.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Registros.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistroPage : ContentPage
	{
        PersonaService Service;
		public RegistroPage (PersonaService persona)
		{
			InitializeComponent ();
            Service = persona;
            grdPersona.BindingContext = new Persona();
            btnRegistar.Clicked += BtnRegistar_Clicked;
		}

        private async void BtnRegistar_Clicked(object sender, EventArgs e)
        {
            Service.Crear(grdPersona.BindingContext as Persona);
            grdPersona.BindingContext = new Persona();
            await DisplayAlert("Aviso", "Persona Agregada Exitosamente", "Ok");
            await Navigation.PushModalAsync(new NavigationPage(new MainPage(Service)));
        }
    }
}