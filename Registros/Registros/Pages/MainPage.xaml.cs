﻿using Registros.Entidades;
using Registros.Models;
using Registros.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Registros.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
        PersonaService Service;
		public MainPage (PersonaService personas=null)
		{
			InitializeComponent ();
            grdLogin.BindingContext = new LoginModel();
            if (personas == null)
            {
                Service = new PersonaService();
            }
            else
            {
                Service = personas;
            }
            btnRegistrar.Clicked += BtnRegistrar_Clicked;
            btnIngresar.Clicked += BtnIngresar_Clicked;
		}

        private async void BtnIngresar_Clicked(object sender, EventArgs e)
        {
            LoginModel model= grdLogin.BindingContext as LoginModel;
            Persona persona = Service.Read.Where(p => p.Password == model.Password && p.Usuario == model.Usuario).SingleOrDefault();
            if (persona != null)
            {
                await DisplayAlert("Aviso", "Usuario Existente", "Ok");
            }
            else
            {
                await DisplayAlert("Error", "Este usuario no existe brother :'v", "Ok");
            }
        }

        private async void BtnRegistrar_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegistroPage(Service));

        }
    }
}