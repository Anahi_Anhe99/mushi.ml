﻿using Registros.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Registros.Servicio
{
    public class PersonaService
    {
        List<Persona> Personas;
        public PersonaService()
        {
            Personas = new List<Persona>();
        }
        public List<Persona> Read
        {
            get
            {
                return Personas;
            }
        }
        public bool Crear(Persona persona)
        {
            persona.Id = Guid.NewGuid().ToString();
            Personas.Add(persona);
            return true;
        }
        public bool Eliminar(Persona persona )
        {
            Personas.Remove(persona);
            return true;
        }
        public bool Actualizar(Persona personModificada)
        {
            Personas.Remove(Personas.Where(p => p.Id == personModificada.Id).SingleOrDefault());
            Personas.Add(personModificada);
            return true;
        }

    }
}
