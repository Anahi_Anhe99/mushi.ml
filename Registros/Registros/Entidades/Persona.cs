﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Registros.Entidades
{
    public class Persona
    {
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string  Password { get; set; }
        public string Id { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
    }
}
